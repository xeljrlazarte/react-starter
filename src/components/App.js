import React from 'react'
import { Switch, Route, Link } from 'react-router-dom'
import { injectGlobal, ThemeProvider } from 'styled-components'
import styles from './css/style.css';

import { HomePage } from 'components'
import { LoginPage } from 'components'

// https://github.com/diegohaz/arc/wiki/Styling
import theme from './themes/default'

injectGlobal`
  body {
    margin: 0;
  }
`

const App = () => {
  return (
    <ThemeProvider theme={theme}>
        <div>
            <nav className={`navbar navbar-default ${styles.margin_bottom_0}`}>
                <div className="container-fluid">
                    <div className="navbar-header">
                        <button type="button" className="navbar-toggle collapsed" data-toggle={'collapse'} data-target={'#myNavbar'} aria-expanded="false">
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                            <span className="icon-bar"></span>
                        </button>
                        <Link className="navbar-brand" to={'/'}>Me</Link>
                    </div>
                    <div className="collapse navbar-collapse" id="myNavbar">
                        <ul className="nav navbar-nav navbar-right">
                            <li><Link to={'/'}>WHO</Link></li>
                            <li><Link to={'/'}>WHAT</Link></li>
                            <li><Link to={'/'}>WHERE</Link></li>
                            <li><Link to={'/Login'}>LOGIN</Link></li>
                        </ul>
                    </div>
                </div>
            </nav>

            <div>
                <Route path="/" component={HomePage} exact />
                <Route path="/Login" component={LoginPage} exact />
            </div>

            <footer className={`text-center ${styles.container_fluid} ${styles.bg_4}`}>
                <h5>ReactJS Theme Made By <a href="https://www.linkedin.com/in/felix-jr-lazarte-90b417124">Xel Jr Lazarte</a></h5>
            </footer>
        </div>
    </ThemeProvider>
  )
}

export default App
