import React from 'react'
import styles from './style.css';

const HomePage = () => {
    return (
        <div>
            <div className={`jumbotron text-center ${styles.bg_1} ${styles.margin_bottom_0}`}>
                <h1>XEL ReactJS Starter app</h1>
                <p>“Design is not just what it looks like and feels like. Design is how it works.”</p>
            </div>
            <div className={`text-center ${styles.container_fluid} ${styles.bg_2}`}>
                <h3 className={styles.margin}>What is ReactJS?</h3>
                <p className={styles.p}>ReactJS basically is an open-source JavaScript library which is used for building user interfaces specifically for single page applications. It’s used for handling view layer for web and mobile apps. React also allows us to create reusable UI components. React was first created by Jordan Walke, a software engineer working for Facebook. React first deployed on Facebook’s newsfeed in 2011 and on Instagram.com in 2012.</p>
                <p className={styles.p}>React allows developers to create large web applications which can change data, without reloading the page. The main purpose of React is to be fast, scalable, and simple. It works only on user interfaces in application. This corresponds to view in the MVC template. It can be used with a combination of other JavaScript libraries or frameworks, such as Angular JS in MVC.</p>
                <a href="#" className="btn btn-default btn-lg">
                    <span className="glyphicon glyphicon-search"></span> Search
                </a>
            </div>
            <div className={`text-center ${styles.container_fluid} ${styles.bg_3}`}>
                <h3 className={styles.margin}>What are the ReactJS Features?</h3>
                <div className="row">
                    <div className="col-sm-3">
                        <h2>JSX</h2>
                        <p className={styles.p}>In React, instead of using regular JavaScript for templating, it uses JSX. JSX is simple JavaScript which allows HTML quoting and uses these HTML tag syntax to render subcomponents. HTML syntax is processed into JavaScript calls of React Framework. We can also write in pure old JavaScript.</p>
                    </div>
                    <div className="col-sm-3">
                        <h2>React Native</h2>
                        <p className={styles.p}>React has native libraries which were announced by Facebook in 2015, which provides the react architecture to native applications like IOS, Android and UPD.</p>
                    </div>
                    <div className="col-sm-3">
                        <h2>Single-Way data flow</h2>
                        <p className={styles.p}>In React, a set of immutable values are passed to the components renderer as properties in its HTML tags. Component cannot directly modify any properties but can pass a call back functionn with help of which we can do modifications. This complete process is known as properties flow down; actions flow up.</p>
                    </div>
                    <div className="col-sm-3">
                        <h2>Virtual Document Object Model</h2>
                        <p className={styles.p}>React creates an in-memory data structure cache which computes the changes made and then updates the browser. This allows a special feature which enable programmer to code as if whole page is render on each change where as react library only render components which actually change.</p>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default HomePage
