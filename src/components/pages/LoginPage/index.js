import React from 'react'
import styles from './style.css'

class LoginPage extends React.Component {
    constructor(props) {
        super(props);
        this.loginEvent = this.loginEvent.bind(this);
    };
    loginEvent() {
        var username = this.userName.value
        var password = this.userPassword.value
        console.log(username+"-"+password);
    }
    render() {
        return (
            <div id={styles.login}>
                <div className={styles.form}>
                    <h3>LOGIN</h3>
                    <input className={styles.text} type="text" placeholder="Username" ref={(input) => this.userName = input}/>
                    <input className={styles.text} type="password" placeholder="Password" ref={(input) => this.userPassword = input}/>
                    <input className={styles.submit} type="submit" onClick={this.loginEvent}/>
                </div>
            </div>
        );
    }
}
export default LoginPage
